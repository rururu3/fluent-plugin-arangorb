# https://www.rubydoc.info/gems/arangorb/1.4.1
class Fluent::ArangoDB3Output < Fluent::BufferedOutput
  Fluent::Plugin.register_output('arangorb', self)

  # お約束っぽいので入れておく
  include Fluent::SetTagKeyMixin
  config_set_default :include_tag_key, false

  # お約束っぽいので入れておく
  include Fluent::SetTimeKeyMixin
  config_set_default :include_time_key, true
  
  # 設定
  config_param :server, :string, :default => 'localhost'
  config_param :port, :integer, :default => 8529
  config_param :database, :string, :default => '_system'
  config_param :collection, :string
  config_param :user, :string, :default => nil
  config_param :password, :string, :default => nil

  def initialize
    super
  end

  # start メソッドが呼ばれる前に呼ばれます
  def configure(conf)
    super
    require 'arangorb'
    require 'msgpack'
  end

  # 起動する際に呼ばれます
  def start
    super
    
    # ArangoDB Default Server設定
    ArangoServer.default_server user: @user, password: @password, server: @server, port: @port
    
    # ArangoDatabase作成
    @myDatabase = ArangoDatabase.new database: @database
    # Databaseがなかったら作る
    if !ArangoServer.databases.include?(@database) then
      @myDatabase.create
    end
    
    # ArangoCollection作成
    @myCollection = @myDatabase[@collection]
    # Collectionがなかったら作る
    if !@myDatabase.collections.include?(@collection) then
      @myCollection.create
    end
  end

  # 終了する際に呼ばれます
  def shutdown
    super
    
    # ArangoRBに終了処理は不要
  end

  def format(tag, time, record)
    record.to_msgpack
  end

  # 書き込む
  def write(chunk)
    records = []
    # chunkリスト分ループ
    chunk.msgpack_each do |record|
      records << record
    end
    ArangoDocument.create collection: @myCollection, database: @myDatabase, body: records
  end
end
