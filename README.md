# fluent-plugin-arangorb

[Fluentd](https://fluentd.org/) match plugin to do something.

Fluentd to ArangoDB plugin

## Installation

### RubyGems

```
$ gem install fluent-plugin-arangorb
```

### Bundler

Add following line to your Gemfile:

```ruby
gem "fluent-plugin-arangorb"
```

And then execute:

```
$ bundle
```

## Configuration

You can generate configuration template:

```
$ fluent-plugin-config-format match arangorb
```

You can copy and paste generated documents here.

#/opt/td-agent/embedded/bin/fluentd -c ./test/fluent.conf -p ./lib/fluent/plugin
#echo '{"a":"hoge", "b": 1}' | /opt/td-agent/embedded/bin/fluent-cat hoge.test

## Copyright

* Copyright(c) 2018- Harasawa Naoya
* License
  * Apache License, Version 2.0
